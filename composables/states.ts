import { Pokemon, Team } from '~~/types'

export const useTeams = () => useState<Team[]>('teams', () => [])

export const useFavourites = () => useState<Pokemon[]>('favourites', () => [])
