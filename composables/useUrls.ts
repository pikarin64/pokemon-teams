interface AllPokemonSettings {
  limit?: number | string
  offset?: number | string
}

export const useUrl = () => {
  const { POKE_API_URL, POKE_SPRITE_URL } = useRuntimeConfig()

  const spriteUrl = (id: number | string) => `${POKE_SPRITE_URL}${id}.png`

  const allPokemonUrl = ({
    limit = 1000,
    offset = 0,
  }: AllPokemonSettings = {}) =>
    `${POKE_API_URL}pokemon?limit=${limit}&offset=${offset}`

  return {
    spriteUrl,
    allPokemonUrl,
  }
}
