# Pokemon Team App using Nuxt 3 beta

Stack used
- [Nuxt 3](https://v3.nuxtjs.org)
- [Tailwind CSS](https://tailwindcss.com)

## Setup

Make sure to install the dependencies

```bash
npm install
```

## Development

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```
