import { defineNuxtConfig } from 'nuxt3'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  publicRuntimeConfig: {
    POKE_API_URL: 'https://pokeapi.co/api/v2/',
    POKE_SPRITE_URL:
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/',
  },
  buildModules: ['@nuxtjs/eslint-module'],
  build: {
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {},
        },
      },
    },
  },
})
