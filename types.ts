interface PokemonType {
  slot: number
  type: {
    name: string
    url: string
  }
}

interface PokemonDetail {
  id: number
  name: string
  types: PokemonType[]
  sprites: {
    front_default: string
  }
}
interface Pokemon {
  id: number
  url?: string
  name: string
  type?: string
}

interface Team {
  id: number
  name: string
  pokemon: PokemonDetail[]
}

interface TeamForm {
  name: string
  pokemon: PokemonDetail[]
}

export { Pokemon, PokemonDetail, Team, TeamForm }
