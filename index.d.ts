declare module '@nuxt/schema' {
  interface PublicRuntimeConfig {
    POKE_API_URL: string
    POKE_SPRITE_URL: string
  }
}
// It is always important to ensure you import/export something when augmenting a type
export {}
